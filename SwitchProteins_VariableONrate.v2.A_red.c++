#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<fstream>
#include<sstream>
#include<vector>
#include<ctime>
#include <unistd.h>

using namespace std;


// Define the max number for chromatin beads and protein beads and velocity types
const int N=1;
const int Nbeadsmax=1000;
const int Nprotmax=4000;
const int Ntotmax=2*Nbeadsmax+3*Nprotmax;

// Ausiliary variable for putting trash when importing data
string dummy;

// If == 1 --> Output printed on screen
int speak=0;



// Function that generates the ON rate (depending on position)
double rateF(double r[3], double A, double l, int Nbeads, double ChromatinP[][9], double Lx, double Ly, double Lz);




//--------------//
//--------------//
// MAIN PROGRAM //
//--------------//
//--------------//



int main(int argc, char* argv[]){

//-------------------
// Define variables
//-------------------


// Total number of atoms
// Ntot = 2 * Nbeads + 3 * Nprot;
int Ntot;

// Number of chromatin beads
int Nbeads;

// Number fo SAF-A proteins
int Nprot;

// Number of different atom types
int ntype;

// Number of angles and angle types
int nangles, nangletypes;

// Number of bonds and bond types
int nbonds,nbondtypes;

// Size of simulation box
double Lmaxx,Lminx,Lmaxy,Lminy,Lmaxz,Lminz;
double Lx,Ly,Lz;

// SAF-A beads
double Protein[Nprotmax][9]; //x,y,z,mol,type,id,ix,iy,iz

// First SAF-A patch
double Protein1[Nprotmax][7]; //x,y,z,mol,type,id,flag
int Protein1Img[Nprotmax][3]; //ix,iy,iz

// Second SAF-A patch
double Protein2[Nprotmax][7]; //x,y,z,mol,type,id,flag
int Protein2Img[Nprotmax][3]; //ix,iy,iz

// Chromatin beads
double Chromatin[Nbeadsmax][9]; //x,y,z,mol,type,id,ix,iy,iz

// Chromatin patch
double ChromatinP[Nbeadsmax][9]; //x,y,z,mol,type,id,ix,iy,iz

// Velocities
double Velocity[N][2*Nbeadsmax+3*Nprotmax][3];

// Bonds
double bond[Nbeadsmax][3];

// Angles
double angle[Nbeadsmax][4];



//--------------------
// Import variables
//--------------------
	
// Time
int mytime=atoi(argv[1]);

// Argv[2] --> input file

// Argv[3] --> Output file

// Rate(probability) of turning SAF-A OFF
double rateOFF=atof(argv[4]);

// Decay length of RNA field
// This parameter comes out by computing the typical diffusion length l=Sqrt[D/k_deg]
// With k_deg=1/hour (from RyuSuke) and D=kBT/3pi.eta.sigma=0.22 um^2/s -> L ~ 28 um = 2800 sigma
// This is true for FREE RNA diffusion, but perhaps caRNA is bound/slower
// !!! Update values !!!
double LoffRNA=atof(argv[5]); //in units of 10 nm

// Seed
double seed=atoi(argv[6]);
srand(seed);

// Number of SAF-A proteins
Nprot=atoi(argv[7]);

// Number of chromatin beads
Nbeads=atoi(argv[8]);

// Gene activity A=kon/(4piD)
double A=atof(argv[9]);

// Final gene activity
double Af=atof(argv[10]);

// Time to start decrease
double time_st=atoi(argv[11]);

// Time to end decrease
double time_end=atoi(argv[12]);

// Initialise arrays
for(int n=0;n<Nprot;n++)for(int c=0;c<7;c++)Protein1[n][c]=Protein2[n][c]=0;
for(int n=0;n<Nprot;n++)for(int c=0;c<3;c++)Protein1Img[n][c]=Protein2Img[n][c]=0;
for(int n=0;n<Nprot;n++)for(int c=0;c<9;c++)Protein[n][c]=0;
for(int n=0;n<Nbeads;n++)for(int c=0;c<9;c++)Chromatin[n][c]=0;

//--------------------------
// READ THE INPUT DATAFILE
//--------------------------

// .clear() without arguments can be used to unset the failbit after unexpected input
ifstream indata;

// Initialise readFile string
stringstream readFile;

//Define readFile string as empty
readFile.str("");

//Erases the contents of the string, which becomes an empty string
// .clear() without arguments can be used to unset the failbit after unexpected input
readFile.clear();

// Input file + mytime is the name of the readFile
readFile << argv[2] << argv[1];

//Open the data in readfile
// .c_str() returns a pointer to an array that contains a null-terminated sequence of characters (i.e., a C-string) representing the current value of the string object.
indata.open(readFile.str().c_str());

// Initialise readFile string
if(!indata.good()){cout << "AHAHAHAHHA"<<endl; cin.get();cin.get();cin.get();}


// 1. READ GENERAL INFO (First 11 lines of the file)
// i=0 is header

for(int i=0;i<=11;i++)
{
 
  if(i==2) 
  {

    indata >> Ntot;
    
    if (speak)
      cout << "Ntot " << Ntot << endl;

  }

  if(i==3)
  {

    indata >> ntype;
    
    if (speak)
      cout << "Ntypes: " << ntype << endl;

  }

  if(i==4)
  {

    indata >> nbonds;
    
    if (speak)
      cout << "Nbonds: " << nbonds << endl;
  
  }

  if(i==5)
  
  {

    indata >> nbondtypes;

    if (speak)
      cout << "Nbondtypes: " << nbondtypes << endl;
  }

  if(i==6)

  {

    indata >> nangles;

    if (speak)
      cout << "Nangles: " << nangles << endl;

  }

  if(i==7)
  {

    indata >> nangletypes;

    if (speak)
      cout << "Nangletypes: " << nangletypes << endl;

  }

  if(i==9) 
  {

    indata >> Lminx >> Lmaxx;
    
    if (speak)
      cout << "L " << Lminx<< " " << Lmaxx <<endl;

    Lx = Lmaxx-Lminx;
    
  }

  if(i==10) 
  {

    indata >> Lminy >> Lmaxy;

    if (speak)
      cout << "L " << Lminy << " " << Lmaxy << endl;

    Ly = Lmaxy-Lminy;
    
  }

  if(i==11) 
  {

    indata >> Lminz >> Lmaxz;

    if (speak)
      cout << "L " << Lminz << " " << Lmaxz <<endl;

    Lz = Lmaxz-Lminz;
   
  }

  // Rest of the line is trash 
  getline(indata,dummy);
 
}


// 2. READ POSITIONS

// Find Atoms positions
// std::string::npos is a -1 constant used to get the position where the string is found
string haystack;
string needle="Atoms";
while(getline(indata,haystack))
{
  if (haystack.find(needle) != std::string::npos)
  { 
 
    break;

  }
}

// Read atoms positions and other info
for(int n=0; n<Ntot; n++)
{
  // Define temporary variables to store info
  int id;
  int mol;
  int type;
  double x,y,z;
  int ix,iy,iz;

  // Save the readings in temporary variables
  indata >> id >> mol >> type >> x >> y >> z >> ix >> iy >> iz;

  // Print
  if(speak)
    cout << n << " entry " << id << " " << mol << " " << type << " "; 

  // If type is 1 or 10 --> data is SAF-A bead
  if(type==1 || type==10)
  {
    
    // Print	  
    if(speak)
      cout << "goes in " << 1 <<endl;

    // First we have chromatin beads, and then SAF-A in the molecule sequence
    int mol1=mol-Nbeads;
    
    //Save x,y,z,mol,type,id,ix,iy,iz
    Protein[mol1-1][0]=x; 
    Protein[mol1-1][1]=y;
    Protein[mol1-1][2]=z;
    
    Protein[mol1-1][3]=mol;
    Protein[mol1-1][4]=type;
    Protein[mol1-1][5]=id; 
    
    Protein[mol1-1][6]=ix;
    Protein[mol1-1][7]=iy;
    Protein[mol1-1][8]=iz;

  }

  // If type is 2 or 3 --> data is SAFA patches
  if(type==2 || type==3)
  {
    //Print
    if(speak)
      cout << "goes in " << 2 <<endl;
    
    // Patch with odd id --> Protein1
    if(id%2==0)
    {
      
      // First we have chromatin beads, and then SAF-A in the molecule sequence
      int mol1=mol-Nbeads;
      
      //Save x,y,z,mol,type,id,ix,iy,iz
      Protein1[mol1-1][0]=x;
      Protein1[mol1-1][1]=y;
      Protein1[mol1-1][2]=z;
    
      Protein1[mol1-1][3]=mol;
      Protein1[mol1-1][4]=type;
      Protein1[mol1-1][5]=id;
    
      Protein1Img[mol1-1][0]=ix;
      Protein1Img[mol1-1][1]=iy;
      Protein1Img[mol1-1][2]=iz;
    }

    // Patch with even id --> Protein2    
    if(id%2==1)
    {

      // First we have chromatin beads, and then SAF-A in the molecule sequence
      int mol1=mol-Nbeads;
    
      //Save x,y,z,mol,type,id,ix,iy,iz
      Protein2[mol1-1][0]=x;
      Protein2[mol1-1][1]=y;
      Protein2[mol1-1][2]=z;
    
      Protein2[mol1-1][3]=mol;
      Protein2[mol1-1][4]=type;
      Protein2[mol1-1][5]=id;
    
      Protein2Img[mol1-1][0]=ix;
      Protein2Img[mol1-1][1]=iy;
      Protein2Img[mol1-1][2]=iz;
    }
  }

  // If type is 4, 5 or 6 --> data is chromatin bead
  if(type==4 || type==5 || type==6)
  {
  
    // Print
    if(speak)
      cout << "goes in " << 3 <<endl;

    //Save x,y,z,mol,type,id,ix,iy,iz
    Chromatin[mol-1][0]=x;
    Chromatin[mol-1][1]=y;
    Chromatin[mol-1][2]=z;
    Chromatin[mol-1][3]=mol;
    Chromatin[mol-1][4]=type;
    Chromatin[mol-1][5]=id;
    Chromatin[mol-1][6]=ix;
    Chromatin[mol-1][7]=iy;
    Chromatin[mol-1][8]=iz;
  }

  // If type is 7 or 8 --> data is chromatin patch 
  if(type==7 || type==8)
  {
  
    // Print
    if(speak)
      cout << "goes in " << 4 <<endl;

    //Save x,y,z,mol,type,id,ix,iy,iz
    ChromatinP[mol-1][0]=x;
    ChromatinP[mol-1][1]=y;
    ChromatinP[mol-1][2]=z;
    ChromatinP[mol-1][3]=mol;
    ChromatinP[mol-1][4]=type;
    ChromatinP[mol-1][5]=id;
    ChromatinP[mol-1][6]=ix;
    ChromatinP[mol-1][7]=iy;
    ChromatinP[mol-1][8]=iz;
   }
}
    
 
// 3. READ VELOCITIES

// Get to the line of velocities
string dummy2;
getline(indata,dummy2);
getline(indata,dummy2);
getline(indata,dummy2);
getline(indata,dummy2);




// Loop over all atoms    
for(int n=0; n<Ntot; n++)
{

  // Define temporary variables to store info  
  double vx,vy,vz;
  int id;

  // Save the readings in temporary variables
  indata >> id >> vx >> vy >> vz;
  
  if (speak)
  cout << id << " v " << vx << endl;

  // Save velocities
  Velocity[0][id-1][0]=vx;
  Velocity[0][id-1][1]=vy;
  Velocity[0][id-1][2]=vz;
}


// 4. READ BONDS

// Get to the line of bonds
getline(indata,dummy2);
getline(indata,dummy2);
getline(indata,dummy2);


// Loop over all bonds
for(int n=0; n<nbonds; n++)
{

  // Define temporary variables to store info  
  int btype,b1,b2,id;

  // Save the readings in temporary variables       
  indata >> id >> btype >> b1 >> b2;
  
  if (speak)
  cout << id << " b " << b1 << endl;

  // Save bonds   
  bond[id-1][0]=btype;
  bond[id-1][1]=b1;
  bond[id-1][2]=b2;

}


// 5. READ ANGLES

// Get to the line of angles
getline(indata,dummy2);
getline(indata,dummy2);
getline(indata,dummy2);
getline(indata,dummy2);



// Loop over angles
for(int n=0; n<nangles; n++)
{

  // Define temporary variables to store info
  int atype,a1,a2,a3,id; 
  
  // Save the readings in temporary variables
  indata >> id >> atype >> a1 >> a2 >> a3;

  if (speak)
  cout << id << " a " << a3 <<endl;

  // Save angles 
  angle[id-1][0]=atype;
  angle[id-1][1]=a1;
  angle[id-1][2]=a2;
  angle[id-1][3]=a3;

}


//--------------------------
// SWITCHING PART
//--------------------------


// Define variables
// number proteins on, off and changes
int non=0;
int noff=0;
int nchanges=0;


// Define starting number of on and off proteins
// Loop over all proteins
for(int n=0; n<Nprot; n++)
{

  // If protein is off, increase the number of off
  if (Protein1[n][4]==3)
    noff++;

  // If protein is on, increase the number of on
  if (Protein1[n][4]==2)
    non++;
}

if (speak)
{

  cout << "Start:" << endl;
  cout << "1.Switched "<<  nchanges << " proteins"<< endl;
  cout << "1.Non "<<  non << " Noff " << noff<< endl;

}


// 1. TURN OFF PROCESS
// The SWITCH OFF rate is thought to be proportional to be the ATP-hydrolisis rate and is constant.
// !!! write rate off constant

// Loop over all proteins
for(int n=0; n<Nprot; n++)
{

  // Generate random number between 0 and 1
  double p=rand()*1.0/RAND_MAX;
    
  // Accept protein turning off if p < rateOFF
  if(p<rateOFF)
  {
    
    // If protein is ON (patch type is 2) --> OFF (both patches get type 3)  -- no change if OFF -- 
    if(Protein1[n][4]==2)
    {
      // Change the type of the patches --> 3
      Protein1[n][4]=Protein2[n][4]=3; 

      // Change the body id -->10
      Protein[n][4]=10;
   
      // Increase the number of changes and off, decrease number of on
      nchanges++;
      noff++;
      non--;
     
    }
  }
}

// Print number of on, off and switches
if (speak)
{

  cout << "After switching OFF:" << endl;
  cout << "2.Switched "<<  nchanges << " proteins"<< endl;
  cout << "2.Non "<<  non << " Noff " << noff<< endl;

}



// 2. TURN ON PROCESS
// Here we use a SWTICH ON rate that is a funciton of relative position from active regions.
// We use a Yukawa function to mimic Diffusion of RNA with source at transcription site
// and degradation at a cartain rate kdeg.
// kdeg does not have to be strictly related to the SAF-A switch off rate.
// We typically use the turnover time of caRNA (from Ryu-Suke) ~ 1h


// Setting the current A value

if (mytime>time_st && mytime<time_end)
  A=(Af-A)*(mytime-time_st)/(time_end-time_st)+A;

if (mytime>=time_end)
  A=Af;

// Loop over all the proteins
for(int n=0; n<Nprot; n++)
{

  // Save protein position in a ausiliary variable;
  double r[3]; 
  r[0]=Protein[n][0];
  r[1]=Protein[n][1];
  r[2]=Protein[n][2];
  
  // Generate random number between 0 and 1
  double p=rand()*1.0/RAND_MAX;

  // Calculate the probability of turning on (based on r, A and LoffRNA)
  double variablerate=rateF(r,A,LoffRNA,Nbeads,ChromatinP,Lx,Ly,Lz);  
   
  // Accept the switch ON if p<variablerate
  if(p<variablerate)
  { 
    
    // If protein is OFF (patch type is 3) --> ON (both patches get type 2)  -- no change if ON -- 
    if(Protein1[n][4]==3)
    {
    
      // Change the type of the patches --> 2
      Protein1[n][4]=Protein2[n][4]=2;

      // Change the body id --> 1
      Protein[n][4]=1;
      
      // Increase the number of changes and on, decrease number of off
      nchanges++;
      non++;
      noff--;
 
    }
  }
} 

// Print number of on, off and switches
if (speak)
{
  cout << "After switching ON:" << endl;
  cout << "2.Switched "<<  nchanges << " proteins"<< endl;
  cout << "2.Non "<<  non << " Noff " << noff<< endl;

}



//---------------------
// WRITE NEW FILE
//---------------------


// 1. Protein state file (on/off)

// Define string variable
stringstream writeFileP;

// Define file name
writeFileP << "Proteins_" << argv[2] << "dat";

// Open file for writing
ofstream writeP(writeFileP.str().c_str(),std::ios_base::app);

// Write time and number of on and off proteins
writeP << "Time: " << mytime << "  ON: " <<  non << "  OFF: " << noff << " A: " << A << endl;

// Close output ifile
writeP.close();



// 2. New lammps data file 

// Define string variable
stringstream writeFile;

// Define file name
writeFile << argv[3] << argv[1];

// Open file for writing
ofstream write(writeFile.str().c_str());

// Print on screen
if (speak)
  cout << "writing on .... " << writeFile.str().c_str()<<endl;

// Define string precision
write.precision(30);

// Print LAMMPS header
write << "LAMMPS data file via write_data, version 29 Oct 2020, timestep = " << argv[1] << endl;
write << endl;
write << Ntot << " atoms " << endl;
write << ntype << " atom types " << endl;
write << nbonds << " bonds " << endl;
write << nbondtypes << " bond types " << endl;
write << nangles << " angles " << endl;
write << nangletypes << " angle types " << endl;
write << "\n";
write << Lminx << " " << Lmaxx << " xlo xhi" << endl;
write << Lminy << " " << Lmaxy << " ylo yhi" << endl;
write << Lminz << " " << Lmaxz << " zlo zhi" << endl;
write << "\nMasses \n" << endl;
for(int j=0; j<ntype;j++) 
  write << j+1 << " " << 1 << endl; 

// Print Atoms positions
write << "\nAtoms \n"<<endl;
//chromatin + patch
for(int n=0; n<Nbeads;n++)
{
  write << Chromatin[n][5] << " " << Chromatin[n][3] << " " <<  Chromatin[n][4] << " " << Chromatin[n][0] << " " <<Chromatin[n][1] << " " << Chromatin[n][2] << " " << Chromatin[n][6] << " " << Chromatin[n][7] << " " << Chromatin[n][8] << endl;
  write << ChromatinP[n][5] << " " << ChromatinP[n][3] << " " <<  ChromatinP[n][4] << " " << ChromatinP[n][0] << " " <<ChromatinP[n][1] << " " << ChromatinP[n][2] << " " << ChromatinP[n][6] << " " << ChromatinP[n][7] << " " << ChromatinP[n][8] << endl;
}
// protein + patch1 + patch2
for(int n=0; n<Nprot;n++)
{
  write << Protein[n][5] << " " << Protein[n][3] << " " <<  Protein[n][4] << " " << Protein[n][0] << " " <<Protein[n][1] << " " << Protein[n][2] << " " << Protein[n][6] << " " << Protein[n][7] << " " << Protein[n][8] << endl;
  write << Protein1[n][5] << " " << Protein1[n][3] << " " <<  Protein1[n][4] << " " << Protein1[n][0] << " " <<Protein1[n][1] << " " << Protein1[n][2] << " " << Protein1Img[n][0] << " " << Protein1Img[n][1] << " " << Protein1Img[n][2] << endl;
  write << Protein2[n][5] << " " << Protein2[n][3] << " " <<  Protein2[n][4] << " " << Protein2[n][0] << " " <<Protein2[n][1] << " " << Protein2[n][2] << " " << Protein2Img[n][0] << " " << Protein2Img[n][1] << " " << Protein2Img[n][2] << endl;
}


// Print Velocities
write << "\n\n\nVelocities\n" << endl;
for(int n=0; n<Ntot;n++) 
  write << n+1 << " " << Velocity[0][n][0] << " "<< Velocity[0][n][1] << " "<<  Velocity[0][n][2] << endl;

// Print Bonds
write << "\n\n\nBonds\n"<<endl;
for(int n=0; n<nbonds;n++) 
  write << n+1 << " " << bond[n][0] << " "<< bond[n][1] << " " << bond[n][2] << endl;

// Print Angles
write << "\n\n\nAngles\n"<<endl;
for(int n=0; n<Nbeads-2;n++) 
  write << n+1 << " " << angle[n][0] << " "<< angle[n][1] << " " << angle[n][2]<<" " << angle[n][3]<<endl;

// Print on screen
cout << " fin " << endl;

// Close output file
write.close();

// Close input file
indata.close();

return 0;
}

//---------------------//
//---------------------//
// END OF MAIN PROGRAM //
//---------------------//
//---------------------//







//-------------//
//-------------//
// FUNCTIONS   //
//-------------//
//-------------//



//---------------------
// 1. ON RATE FUNCTION
//---------------------

// This is the function that given a certain polymer conformation
// computes the ON RATE as a function of the position in 3D.
// D is the diffusion coefficient, k_deg the rate of degradation of RNA.
// The PDE for RNA is:
// d\rho_RNA /dt = D \nabla^2 \rho_RNA + kon \delta(r-r_{trx}) - kdeg \rho_RNA
// whose steady state is described by:
// \nabla^2 \rho_RNA=- kon/D \delta(r-r_{trx}) + koff/D \rho_RNA
// whose solution is
// \rho_RNA(r)=sum_{r_{trx} A exp(-(r-r_{trx})/Lrna)/r
//
// where A=kon/4*pi*D and Lrna=sqrt(D/kdeg)


double rateF(double r[3], double A, double l, int Nbeads, double ChromatinP[][9], double Lx, double Ly, double Lz)
{

  // RNA field  
  double rhorna=0.0;

  // Function value
  double fun=0.0;

  //position of SAFA
  double xr=r[0];
  double yr=r[1];
  double zr=r[2];

  // Distances Chromatin bead -- SAF-A
  double dx;
  double dy;
  double dz;

  // Probability of producing RNA
  // (typically one polymerase every 100nm (=every 10 beads) --> 10%)
  double rp=0.1;

  // Loop over all chromatin beads
  for(int i=0;i<Nbeads;i++)
  {

    //position of Chromatin patch
    double x=ChromatinP[i][0];
    double y=ChromatinP[i][1];
    double z=ChromatinP[i][2];

    // We consider a chromatin bead as RNA producer:
    // 1. if active (patch is type 7)
    // 2. with a prob rp (depending on polymerase activity)
    if(ChromatinP[i][4]==7 && rand()*1.0/RAND_MAX < rp)
    {

      // Distances by taking into account boundary conditions    
      dx=xr-x;
      if(dx<=-Lx/2.)
        dx+=Lx;
      if(dx>Lx/2.)
        dx-=Lx;
      
      dy=yr-y;
      if(dy<-Ly/2.)
	dy+=Ly;
      if(dy>Ly/2.)
	dy-=Ly;
      
      dz=zr-z;
      if(dz<-Lz/2.)
	dz+=Lz;
      if(dz>Lz/2.)
	dz-=Lz; 
    
      // Calculate the distance between the chromatin beads and the protein
      double d=pow(dx*dx+dy*dy+dz*dz,0.5);
      
      // Calculate the value of the RNA field
      rhorna+=A*(exp(-d/l)/d);
  
    }
  }

  // We need to write a function of the concentration of rna
  // that describes the probability of turning a SAF-A mol ON.
  // This can be something like min{1,rhorna}
  
  double arbitraryconst=1.0;
  
  if(rhorna*arbitraryconst<1.0)
    fun=rhorna*arbitraryconst;
  else 
    fun=1.0;

  return fun;
}
