SAFA_RNAfield

this is an example LAMMPS run.
To run the simulation:

1. install LAMMPS
2. compile the c++ code SwitchProteins_VariableONrate.v2.A_red.c++

3. execute ~/lmp -in Run.Gel.Chrom.v2.A_red.lam

the LAMMPS script will automatically create output folder for the simulation trajectory and will call the external c++ code to "switch" SAFA proteins on/off.

More explanations are given in the paper XXX and in the codes.
