##############
# PARAMETERS #
##############

# Proteins
variable N equal 1000 # proteins

# Chromatin beads
variable Nc equal 1000 # chromatin

# Binding energy for Morse Potential
variable E equal 40.0

# Turnover time (in seconds) and rate (in 1/seconds) 
# (default turnover time is tt=0.6 seconds)
# Note that by using in simulations a timestep=0.002 (in LJ units), if SIGMA=10nm -> tau_D=0.5ms (eta=200cP) -> 10^3tau=5*10^5steps=0.5s~1s
# k is therefore the probability of changing state every 500.000 steps (=1s).
variable tt equal 0.6
variable k equal 1.0/${tt}

# Decay Length for RNA ( Lrna=sqrt(D_RNA)
# (default decay length is 10 )
variable Lrna equal 20

# Gene Activity for the RNA field at full expression
# (A goes from 0.1 to 5 in previuous sims)
variable A equal 5

# Gene Activity when the expression is reduced
variable Af equal 1

# Start time for the reduction of expression
variable t_st equal 20000000

# End time for the reduction of expression
variable t_end equal 40000000

# Replica of the simulation
variable rep equal 10

#Seeds
variable seed0 equal 50139
variable seed2 equal 13705
variable seed3 equal 25593


#########################################
### FOLDERS AND FILE NAMES DEFINITION ###
#########################################

variable datafolder index restart_data
shell mkdir ${datafolder}
variable datafolder2 index dump_data
shell mkdir ${datafolder2}

variable SimOutType index SafaGel_A${A}_lRNA${Lrna}_toff${tt}_rep${rep}

# Starting configuration
variable rname index Start_config/LammpsInput.Chrom+SAFA.r${rep}

# Simulation name
variable simname index ${SimOutType}

#
variable thermofile index Thermo.${SimOutType}



##########################################
###                                    ###
### 1. FIRST THREE EQUILIBRATION STEPS ###
###    (soft+bonds+rigidity)           ###
###                                    ###
##########################################

# Simulation standard settings
units lj
atom_style angle 
boundary      p p p 
neighbor 2.0 bin
neigh_modify every 2 delay 2 check yes

# Setting restarts
restart 10000000 Restart.${simname}.

# Reading starting configuration
read_data ${rname}

####################################
### 	GROUPS DEFINITION	####
####################################


# Proteins group definition

# SAF-A body bead is type 1 or 10
# 10 is when SAF-A is OFF and 1 is when SAF-A is ON
group safa type 1 10

# SAF-A patch in ON state is type 2
group on type 2

# SAF-A patch in OFF state is type 3
group off type 3

# The group proteins is all previous groups together
group proteins type 1 2 3 10


# Chromatin group definition

# Promoter chromatin region (start active region)
group prm type 4

# Active chromatin region (producer of RNA)
group trx type 5

# Heterochromatin region (not active)
group het type 6

# Patchy particles on active chromatin (caRNA)
group pchrom1 type 7

# Patchy particle on heterochromatin
group pchrom2 type 8

# The group chromatin is all previous groups together
group chromatin type 4 5 6 7 8

# There is a type 9 defined but it is not used
# Ask Davide!


####################################
####    BONDS (HARMONIC)         ###
####################################
bond_style harmonic
bond_coeff 1 200 1.1

####################################
###   ANGLE (BENDING RIGIDITY)   ###
####################################
angle_style   cosine
angle_coeff  1 20.0  #20 sigma for realistic DNA (1 bead = 2.5 nm)

####################################
###     POTENTIAL DEFINITION	 ###
####################################

# Soft potential 
pair_style  soft 1.5

# The potential is active among "beads" particles:
# SAF-A inactive (1), SAF-A active (10), chromatin core types (4, 5, 6) 
pair_coeff * * 1.0 1.12246152962189
pair_coeff 1 * 1.0 1.12246152962189

pair_coeff 1 2 0.0 0.0
pair_coeff 1 3 0.0 0.0
pair_coeff 2 * 0.0 0.0
pair_coeff 3 * 0.0 0.0
pair_coeff * 7 0.0 0.0
pair_coeff * 8 0.0 0.0
pair_coeff 7 10 0.0 0.0
pair_coeff 8 10 0.0 0.0

pair_coeff 1 10 1.0 1.12246152962189
pair_coeff 4 10 1.0 1.12246152962189
pair_coeff 5 10 1.0 1.12246152962189
pair_coeff 6 10 1.0 1.12246152962189
pair_coeff 10 10 1.0 1.12246152962189

# The pre-factor of the potential linearly increase from 0 to 500
variable prefactor equal ramp(0,500)
fix pushapart all adapt 1 pair soft a * * v_prefactor

####################################
###	FIXES	                 ###
####################################
# Langevin with rigid bodies made of SAF-A molecules (bead+2patches)
fix 1 proteins rigid/small molecule langevin 1.0 1.0  3.0  ${seed0}

# Langevin with rigid bodies made of chromatin molecules (bead+1patch)
fix 2 chromatin rigid/small molecule langevin 1.0 1.0  2.0  ${seed2}

# This command turns off interactions within same molecule (rigidbody) in group proteins
neigh_modify exclude molecule/intra proteins

# This command turns off interactions within same molecule (rigidbody) in group chromatin
neigh_modify exclude molecule/intra chromatin

####################################
####	DUMPS	             #######
####################################
##XYZ
#dump 4 all xyz 100000 XYZpre_${simname}.xyz
#dump_modify 4 element C Ca O Au Ag N S P Xe Xe

####################################
###	RUN -- EQUILIBRATION     ###
####################################

# Time-step definition for equilibration
timestep 0.01


# FIRST EQUILIBRATION (10000 steps w/ soft potential + bonds + strong bending rigidity)
reset_timestep 0
thermo 1000
thermo_style   custom   step  temp  epair emol vol cpu
run 10000


# SECOND EQUILIBRATION (10000 steps w/ soft potential + relaxing bonds + strong bending rigidity)
# (We don't actually relax bonds)
bond_style harmonic
bond_coeff 1 200 1.1
run 10000


# THIRD EQUILIBRATION (1000000 steps w/ soft potential + relaxing bonds + relaxing bending rigidity)
angle_style cosine
angle_coeff  1 3.0
run 1000000



##########################################
###                                    ###
### 2. FOURTH EQUILIBRATION STEP       ###
###    (LJ+bonds+rigidity+Morse)       ###
###                                    ###
##########################################


####################################
###     POTENTIAL DEFINITION     ###
####################################

# Morse parameters
variable Emorse equal ${E}
variable Rmorse equal 16

# Chromatin self-attraction parameters
variable Eprm equal 3.0 #strongly self-sticky->loops
variable Etxr equal 0.9 #weakly self-sticky
variable Ehet equal 0.9 #weakly self-sticky

# Hybrid style: Morse + LJ
pair_style  hybrid lj/cut 1.3 morse 1.0
pair_modify     shift yes

# Define all pairs with LJ (it will be overwritten)
pair_coeff * * lj/cut 1.0 1.0 1.12246152962189

# SAF-A bead OFF (type 1)
# LJ with itself (1), chromatin beads (4,5,6) and active SAF-A (10)
pair_coeff 1 1 lj/cut 1.0 1.0 1.12246152962189
pair_coeff 1 2 lj/cut 0.0 0.0 0.0
pair_coeff 1 3 lj/cut 0.0 0.0 0.0
pair_coeff 1 4 lj/cut 1.0 1.0 1.12246152962189
pair_coeff 1 5 lj/cut 1.0 1.0 1.12246152962189
pair_coeff 1 6 lj/cut 1.0 1.0 1.12246152962189
pair_coeff 1 7 lj/cut 0.0 0.0 0.0
pair_coeff 1 8 lj/cut 0.0 0.0 0.0
pair_coeff 1 9 lj/cut 0.0 0.0 0.0
pair_coeff 1 10 lj/cut 1.0 1.0 1.12246152962189

# SAF-A patch ON (type 2)
# Morse attraction with itself (2) and active chromatin patches (7) 
pair_coeff 2 2 morse ${Emorse} ${Rmorse} 0.0 1.0
pair_coeff 2 3 lj/cut 0.0 0.0 0.0
pair_coeff 2 4 lj/cut 0.0 0.0 0.0
pair_coeff 2 5 lj/cut 0.0 0.0 0.0
pair_coeff 2 6 lj/cut 0.0 0.0 0.0
pair_coeff 2 7 morse ${Emorse} ${Rmorse} 0.0 1.0 
pair_coeff 2 8 lj/cut 0.0 0.0 0.0
pair_coeff 2 9 lj/cut 0.0 0.0 0.0
pair_coeff 2 10 lj/cut 0.0 0.0 0.0

# SAF-A patch OFF (type 3)
# Morse attraction with with active chromatin patches (7). Coating the region.
pair_coeff 3 3 morse 0.0 ${Rmorse} 0.0 1.0          
pair_coeff 3 4 lj/cut 0.0 0.0 0.0
pair_coeff 3 5 lj/cut 0.0 0.0 0.0
pair_coeff 3 6 lj/cut 0.0 0.0 0.0
pair_coeff 3 7 morse ${Emorse} ${Rmorse} 0.0 1.0
pair_coeff 3 8 lj/cut 0.0 0.0 0.0
pair_coeff 3 9 lj/cut 0.0 0.0 0.0
pair_coeff 3 10 lj/cut 0.0 0.0 0.0

# Chromatin body - Promoter (type 4)
# LJ attraction with itself (4), repulsion with other chromatin beads (5,6) and active SAF-A (10)
pair_coeff 4 4 lj/cut ${Eprm} 1.0 1.8  
pair_coeff 4 5 lj/cut 1.0 1.0 1.12246152962189
pair_coeff 4 6 lj/cut 1.0 1.0 1.12246152962189 
pair_coeff 4 7 lj/cut 0.0 0.0 0.0              
pair_coeff 4 8 lj/cut 0.0 0.0 0.0               
pair_coeff 4 9 lj/cut 1.0 1.0 1.12246152962189  
pair_coeff 4 10 lj/cut 1.0 1.0 1.12246152962189  

# Chromatin body - Active chromatin (type 5)
# LJ attraction with itself (5), repulsion with other chromatin beads (6) and active SAF-A (10)
pair_coeff 5 * lj/cut 1.0 1.0 1.12246152962189 
pair_coeff 5 5 lj/cut ${Etxr} 1.0 1.8 
pair_coeff 5 6 lj/cut 1.0 1.0 1.12246152962189  
pair_coeff 5 7 lj/cut 0.0 0.0 0.0              
pair_coeff 5 8 lj/cut 0.0 0.0 0.0             
pair_coeff 5 9 lj/cut 0.0 0.0 0.0 
pair_coeff 5 10 lj/cut 1.0 1.0 1.12246152962189  

# Chromatin body - Heterochromatin (type 6)
# LJ attraction with itself (6), repulsion with active SAF-A (10)
pair_coeff 6 6 lj/cut ${Ehet} 1.0 1.8  		
pair_coeff 6 7 lj/cut 0.0 0.0 0.0               
pair_coeff 6 8 lj/cut 0.0 0.0 0.0               
pair_coeff 6 9 lj/cut 0.0 0.0 0.0  
pair_coeff 6 10 lj/cut 1.0 1.0 1.12246152962189 

# Patch on active chromatin (type 7)
# No other interactions
pair_coeff 7 * lj/cut 0.0 0.0 0.0

# Patch on heterochromatin + promoters (type 8)
# No other interactions
pair_coeff 8 * lj/cut 0.0 0.0 0.0

# Type 9 (we don't use it)
pair_coeff 9 9 lj/cut 0.0 0.0 0.0
pair_coeff 9 10 lj/cut 1.0 1.0 1.12246152962189

# SAF-A bead ON
pair_coeff 10 10 lj/cut 1.0 1.0 1.12246152962189

####################################
###     RUN -- EQUILIBRATION     ###
####################################
unfix pushapart
timestep 0.002
run 10000




##########################################
###                                    ###
### 3. START SIMULATIONS W/ SWITCHING  ###
###  (Loop for changing SAF-A state)   ###
###                                    ###
##########################################


# Remove existing thermo-file or XYZ file
shell rm ${thermofile}
shell rm XYZ.${simname}.xyz

# k is the probability for SAF-A patches to change state every 500.000 steps (~1s).
# Instead of changing the state every second with probability k, with change it every second/smooth w/ probability k/smooth 
variable smooth equal 10
variable runtime equal 500000/${smooth}
variable rate equal ${k}/${smooth}

# Define data restarts    
variable wname index dataRestart.${simname}.
variable newname index newdataRestart.${simname}.

# Write state at time=0
write_data ${wname}0

# Remove existing files that says how many proteins are ON and OFF
shell rm Proteins_${wname}dat

# Run a c++ program that switch SAF-A in ON and OFF state
# Input needed: data file, name of new data file, rate of change, RNA Decay Length, random seed, number of SAF-A proteins, Gene Activity
# Output: Protein file, new data file
shell ./SwitchProteins_VariableONrate.v2.A_red 0 ${wname} ${newname} ${rate} ${Lrna} ${seed3} ${N} ${Nc} ${A} ${Af} ${t_st} ${t_end}

# Move old data file (before switching) in the data folder
shell mv ${wname}0 ${datafolder}/${wname}0


# Start loop!
# Every run time is 500.000/smooth and it is run 800 times 
variable nloops equal 1600
variable a loop ${nloops}
label start_loop

# Define the actual time before run
variable mytime equal (${a}-1)*${runtime}

# Print current runtime
shell echo ${mytime}

# Define new seeds needed within the loop
variable seeda0 equal (${seed0})*((${a}+4))
variable seeda2 equal (${seed2})*((${a}+89))
variable seeda3 equal (${seed3})*((${a}+89))

# Clear everything excepts variables
clear

# Simulation standard settings
units lj
atom_style angle 
boundary        p p p
neighbor 1.0 bin
neigh_modify every 5 delay 5 check yes 

# Define restarts 
restart 50000000 Restart.${simname}.

# Read data from the new data file generated after switching (either at time 0 or at the end of the loop)
variable rnewname index newdataRestart.${simname}.
read_data ${rnewname}${mytime}

# Move the data file in the datafolder
shell mv ${rnewname}${mytime} ${datafolder}/${rnewname}${mytime}


####################################
###     GROUPS DEFINITION       ####
####################################

# Proteins group definition

# SAF-A body bead is type 1 or 10
# 10 is when SAF-A is OFF and 1 is when SAF-A is ON
group safa type 1 10

# SAF-A patch in ON state is type 2
group on type 2

# SAF-A patch in OFF state is type 3
group off type 3

# The group proteins is all previous groups together
group proteins type 1 2 3 10


# Chromatin group definition

# Promoter chromatin region (start active region)
group prm type 4

# Active chromatin region (producer of RNA)
group trx type 5

# Heterochromatin region (not active)
group het type 6

# Patchy particles on active chromatin (caRNA)
group pchrom1 type 7

# Patchy particle on heterochromatin
group pchrom2 type 8

# The group chromatin is all previous groups together
group chromatin type 4 5 6 7 8

# There is a type 9 defined but it is not used
# Ask Davide!



####################################
###     POTENTIAL DEFINITION     ###
####################################

# Morse parameters
variable Emorse equal ${E}
variable Rmorse equal 16

# Chromatin self-attraction parameters
variable Eprm equal 3.0 #strongly self-sticky->loops
variable Etxr equal 0.9 #weakly self-sticky
variable Ehet equal 0.9 #weakly self-sticky

# Hybrid style: Morse + LJ
pair_style  hybrid lj/cut 1.3 morse 1.0
pair_modify     shift yes

# Define all pairs with LJ (it will be overwritten)
pair_coeff * * lj/cut 1.0 1.0 1.12246152962189

# SAF-A bead OFF (type 1)
# LJ with itself (1), chromatin beads (4,5,6) and active SAF-A (10)
pair_coeff 1 1 lj/cut 1.0 1.0 1.12246152962189
pair_coeff 1 2 lj/cut 0.0 0.0 0.0
pair_coeff 1 3 lj/cut 0.0 0.0 0.0
pair_coeff 1 4 lj/cut 1.0 1.0 1.12246152962189
pair_coeff 1 5 lj/cut 1.0 1.0 1.12246152962189
pair_coeff 1 6 lj/cut 1.0 1.0 1.12246152962189
pair_coeff 1 7 lj/cut 0.0 0.0 0.0
pair_coeff 1 8 lj/cut 0.0 0.0 0.0
pair_coeff 1 9 lj/cut 0.0 0.0 0.0
pair_coeff 1 10 lj/cut 1.0 1.0 1.12246152962189

# SAF-A patch ON (type 2)
# Morse attraction with itself (2) and active chromatin patches (7)
pair_coeff 2 2 morse ${Emorse} ${Rmorse} 0.0 1.0   
pair_coeff 2 3 lj/cut 0.0 0.0 0.0
pair_coeff 2 4 lj/cut 0.0 0.0 0.0
pair_coeff 2 5 lj/cut 0.0 0.0 0.0
pair_coeff 2 6 lj/cut 0.0 0.0 0.0
pair_coeff 2 7 morse ${Emorse} ${Rmorse} 0.0 1.0 
pair_coeff 2 8 lj/cut 0.0 0.0 0.0
pair_coeff 2 9 lj/cut 0.0 0.0 0.0
pair_coeff 2 10 lj/cut 0.0 0.0 0.0

# SAF-A patch OFF (type 3)
# Morse attraction with with active chromatin patches (7). Coating the region.
pair_coeff 3 3 morse 0.0 ${Rmorse} 0.0 1.0     
pair_coeff 3 4 lj/cut 0.0 0.0 0.0
pair_coeff 3 5 lj/cut 0.0 0.0 0.0
pair_coeff 3 6 lj/cut 0.0 0.0 0.0
pair_coeff 3 7 morse ${Emorse} ${Rmorse} 0.0 1.0 
pair_coeff 3 8 lj/cut 0.0 0.0 0.0
pair_coeff 3 9 lj/cut 0.0 0.0 0.0
pair_coeff 3 10 lj/cut 0.0 0.0 0.0

# Chromatin body - Promoter (type 4)
# LJ attraction with itself (4), repulsion with other chromatin beads (5,6) and active SAF-A (10)
pair_coeff 4 4 lj/cut ${Eprm} 1.0 1.8 
pair_coeff 4 5 lj/cut 1.0 1.0 1.12246152962189 
pair_coeff 4 6 lj/cut 1.0 1.0 1.12246152962189
pair_coeff 4 7 lj/cut 0.0 0.0 0.0            
pair_coeff 4 8 lj/cut 0.0 0.0 0.0           
pair_coeff 4 9 lj/cut 1.0 1.0 1.12246152962189  
pair_coeff 4 10 lj/cut 1.0 1.0 1.12246152962189

# Chromatin body - Active chromatin (type 5)
# LJ attraction with itself (5), repulsion with other chromatin beads (6) and active SAF-A (10)
pair_coeff 5 * lj/cut 1.0 1.0 1.12246152962189 
pair_coeff 5 5 lj/cut ${Etxr} 1.0 1.8  
pair_coeff 5 6 lj/cut 1.0 1.0 1.12246152962189  
pair_coeff 5 7 lj/cut 0.0 0.0 0.0              
pair_coeff 5 8 lj/cut 0.0 0.0 0.0             
pair_coeff 5 9 lj/cut 0.0 0.0 0.0 
pair_coeff 5 10 lj/cut 1.0 1.0 1.12246152962189  

# Chromatin body - Heterochromatin (type 6)
# LJ attraction with itself (6), repulsion with active SAF-A (10)
pair_coeff 6 6 lj/cut ${Ehet} 1.0 1.8  		
pair_coeff 6 7 lj/cut 0.0 0.0 0.0              
pair_coeff 6 8 lj/cut 0.0 0.0 0.0             
pair_coeff 6 9 lj/cut 0.0 0.0 0.0  
pair_coeff 6 10 lj/cut 1.0 1.0 1.12246152962189  

# Patch on active chromatin (type 7)
# No other interactions
pair_coeff 7 * lj/cut 0.0 0.0 0.0		

# Patch on heterochromatin + promoters (type 8)
# No other interactions
pair_coeff 8 * lj/cut 0.0 0.0 0.0              

# Type 9 (we don't use it)
pair_coeff 9 9 lj/cut 0.0 0.0 0.0
pair_coeff 9 10 lj/cut 1.0 1.0 1.12246152962189

# SAF-A bead ON
pair_coeff 10 10 lj/cut 1.0 1.0 1.12246152962189

####################################
###     BONDS (HARMONIC)         ###
####################################
bond_style harmonic
bond_coeff 1 200 1.1

####################################
###   ANGLE (BENDING RIGIDITY)   ###
####################################
angle_style   cosine
angle_coeff  1 3.0  


####################################
###	FIXES	                 ###
####################################
# Langevin with rigid bodies made of SAF-A molecules (bead+2patches)
fix 1 proteins rigid/small molecule langevin 1.0 1.0  3.0  ${seeda0}

# Langevin with rigid bodies made of chromatin molecules (bead+1patch)
fix 2 chromatin rigid/small molecule langevin 1.0 1.0  2.0  ${seeda2} 

# This command turns off interactions within same molecule (rigidbody) in group proteins
neigh_modify exclude molecule/intra proteins
#
# This command turns off interactions within same molecule (rigidbody) in group chromatin
neigh_modify exclude molecule/intra chromatin

####################################
###	      DUMPS	         ###
####################################

# Define dumptime ( = end of the simulation)
# Why dumptime = runtime +1 ????
variable dumptime equal (${runtime}+1)/1.0

# Variable to decide if printing results every cycle of the loop 
variable dtime equal ${a}%1

## Print XYZ
if "${dtime}==0" then "dump 4 all xyz ${dumptime} XYZ.${simname}.xyz"
if "${dtime}==0" then "dump_modify 4 element C Ca O Au Ag N S P Xe Xe append yes"

# Print DAT
if "${dtime}==0" then "dump 2 all custom ${dumptime} ${datafolder2}/${simname}.${mytime} id type x y z ix iy iz"


####################################
###     RUN -- SIMULATION        ###
####################################

# Print thermodynamic info with corrent time
thermo 10000
thermo_style   custom   step  temp  epair emol vol cpu
variable myenergy equal epair
variable tstep equal ${mytime}
fix p1 all print 10000 "${tstep} ${myenergy} " append ${thermofile} screen no 

# Run simulation
timestep 0.002
run ${runtime} 


#################################################
###  SWITCHING STEP AT THE END OF SIMULATION  ###
#################################################

# Time at the end of simulation within the cycle
variable mytime1 equal ${a}*${runtime}

# Name of data file before and after switch
variable wname index dataRestart.${simname}.
variable newname index newdataRestart.${simname}.

# Write data file
write_data ${wname}${mytime1}

# Switch step as done at time=0, but now it is the total time at the end of the loop
shell ./SwitchProteins_VariableONrate.v2.A_red ${mytime1} ${wname} ${newname} ${rate} ${Lrna} ${seeda3} ${N} ${Nc} ${A} ${Af} ${t_st} ${t_end}

# Move old data file in the data folder
shell mv ${wname}${mytime1} ${datafolder}/${wname}${mytime1}


#################
###   CLEAR   ###
#################

# Clear the data file names, times and all settings
variable newname delete
variable wname delete
variable rnewname delete
variable mytime delete
variable mytime1 delete
clear 

# Jump to the next value in the loop
next a
jump SELF start_loop
